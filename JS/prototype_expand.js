let arr = [1, 2, 3];

if(!Array.prototype.append){
    Array.prototype.append = function(item) { 
        for (let i = this.length - 1; i >=0; i--) {
           this[i +1] = this[i];
        }
        this[0] = item;
        return this;
    };
    
}

console.log(arr.append(0));